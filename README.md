### Установка и настройка компонентов

Установка docker:
```bash
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt update
apt-cache policy docker-ce
sudo apt install docker-ce
sudo systemctl status docker
sudo usermod -aG docker nikita
```

Установка docker-compose:
```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose -v
```

Алиесы:
```bash
alias h='history'
alias dops='docker ps'
alias tapp='cd /home/nikita/test1-app'
alias tdock='cd /home/nikita/test1-docker'
doex ()
{
    docker exec -it "$1" bash
}
```

### Запуск проекта

Запуск контейнеров:
```php
cp .env.example .env
docker-compose up --build -d
```

Войти в любой из контейнеров:
```php
docker exec -it <container_name> bash
```

Посмотреть запущенные контейнеры:
```php
docker ps
```

Логи контейнера:
```php
docker logs <container_name>
```

Остановка контейнеров:
```php
docker stop $(docker ps -a -q)
```

Создание проекта (внутри php-cli):
```php
composer create-project symfony/skeleton:"6.2.*"
```

